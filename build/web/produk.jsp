<%-- 
    Document   : produk
    Created on : Mar 27, 2019, 10:21:33 PM
    Author     : Tis Co
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lihat Produk</title>
         <link href="style.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"> 
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="product.css" rel="stylesheet" type="text/css">
    </head>
    <body>
         <nav class="menum navbar navbar-light navbar-expand-md  absolute-top">
                <div class="container" >
                   <img src="logoyam.png" alt="" width='10%'>
                   <h1 class="judul">Ayam Shop</h1>
                <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
            
                 <ul class="topBotomBordersOut navbar-nav mx-auto text-center">
                    <li class="nav-item active">
                      <a class="nav-link" href="index.jsp">Home</a>
                    </li>
          
                    <li class="nav-item">
                        <div class="search-box">
                                <input type="text" class="search-txt" placeholder="Cari Disini">
                                <a class="search-btn">
                                    <i class="fas fa-search"></i>
                                </a>
                        </div>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="login.jsp">Masuk</a></li>
                    <li class="nav-item"> <a class="nav-link" href="daftar.jsp">Daftar</a> 
                    <li class="nav-item"><button type="button" class="btn btn-default btn-sm">
         <a href="cart.jsp" class="nav-link"> <span class="glyphicon glyphicon-shopping-cart">Keranjang</span></a> 
        </button></li>
                  </ul>
                </div>
                </div>   
              </nav>
<div class="container">
    <div class="card">
        <div class="container-fliud">
            <div class="wrapper row">
                <div class="preview col-md-6">

                    <div class="preview-pic tab-content">
                        <div class="tab-pane active" id="pic-1"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTERUTExIWFRUVFhUWGBUVFRUVFRUVFxUXFxcVFhYYHSggGBolHhUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0lICUtLS4tLS0tLS0tLS0tLS0tLy0tLS0tLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAADBAIFAAEGBwj/xAA/EAABAwIEAwUGAwUIAwEAAAABAAIRAyEEEjFBUWFxBRMigZEGMqGx0fBCUsEUI2Jy4QcVM1OCkrLxQ3PSNP/EABgBAAMBAQAAAAAAAAAAAAAAAAABAgME/8QAJxEAAgICAwABBAEFAAAAAAAAAAECESExAxJBURMyYXEEIjOhwfD/2gAMAwEAAhEDEQA/APDgpNKwNUsioTY2zFEI7cSq0NRXFZOJm0X+Exafbi7Ll8O8pzvysXAStFucUjUcVKoRWTFGvCnqHYvTWUP2hI08QtPqyl1Yuw8cQt9+q3Opiok4i7MdNdYKySD0TMl1FbGv2hYa6ULlpr0uorY13yiaqEXINSqn1AbbVWn10l36A+uhQYx2riVT48hyJUqIL2StYRoFgrHNhY0Jx2HJUqWGhb90brkVG+zKpY5dphK0tXItoK77OrQhcqNI8qaLOuEu16M6pKXcseartGXMltBe8WICxYdjns5PuUxh8LKkRdHovyldfY3ZL+69wkquHg3V9hMYDYpXtACVTqsCK5jFKs1SGqnUZZRY7FqaI4QogQpuNkhEmVDC33pQ2ItNklIknnlTD1LIovpJWgswVFhrKFOgZTtPAlJ0KrAZ7LTHpithiELD0iSBxRgAtCk+ocrGlx4ASuhwX9n+MraMDbSMxjyI1C9D/s99m+4p53hpLr+6ZHr9F39NwAVqHyaLj+TwN39l+PDC7I2R+GbnouZx3s/iKQJqUXtA1JaYF416r6o74Qg4vD06rCx7Q4OEEEbKuq8G4JHyaKKI2ku99v8A2NdhqjqlJn7kna+Wf6rkKdNYytOmYu7A0sMpHDpyixFdRWdgo2VndI9GmnDhlCIRY1FoiJlTeyykQtgpWVWBbKViOtpEUUeLoRdKiV0NShmVViKMOhdcnTN+VJMVo2cnK7JQ2Urp5tOVHZEJoqKrYKLqFZPwUobMNeEuwPDEH07IrKFk7XwRA0RqWHhiXYGV1ChdMijEqGHacxTxp2SbI2KYdklMVaUImGopurh7KbLSwV9OleVaYVqjQpiE5haYTQ4g6uFBWdkYJgxDM9POJ92SL7JqIT2AoOe9uT3vIW3uqWy0keoYeuGtAAiABbQckZuMlUuI/dUwJ0Gy5PtL2ybh6gbUa5rSff2XRRR6Y3EWWquLyx1XG4H2opVR4KrHfyuBKr/af2kHdCmx4FRzm5SZiZ3hEVbA9A7RosrUnNcJBH6LxLEdlhrnAbEj0Xev9oi2gJ/xHNiOB0J6SuVPNZ8rWhJFJ+xXTLMLxTtlGq5YBSoEcMCElWwCsqL1DEElAOvSodShFp4ayO6kZTdJghSskOmV37IsVplCxMnBQ0NUjiqfilWFNl1Cth022TJtoRp0ZVjQoLMHTEpwt8QCExQxkF3QCXbT8SssXSyhIUD4kMqW8lgaQypd9IRATIuEKmdUeFXYDD4ESp1sLKuOwOzHVqrB+GbkXiOIXpVf2Uo1QQ5ga6LPZYG242KuPG5LBKPHWYaEzlmxXS1/ZSvTcQWzB1HDihY/2aqMZni1ttjv981H05LwvRzz6AGig1hCsDhiJB1FlDJClqgBjRXXspSzV2g9fIceSSwVMOXV+xeDDXPfvoOQWvHlofg97RUi6mQF5J7UMqPmnUGYNiXD3p5r2zHUsy5DtvsoVbEHyMT5hdiaQUeJ1cA1kO7wA7Q649E/gMFWa5rjSqv0OYQY5EOC77s32OY2qHWF7uNz6ldL237NBlNr6OZwuXicxk79OSUp0sIbOdGIZWa14lrwMrmukGwsYPopOFkbCYc6lL4p4BhcUnbsTdICyiZlbq0rIzKghazgqbExRtlsvRTSlwR61FoCCcsrqr1lFxKI+heVLJCzzYshMqxYtKx2ikpWKYbDrID41RMOb2RZK3RJtMNcmqcEylHeIrKlbLonHY4q3SGsX4kmylBlTbUJCLhzLoRJZHJZyH7Mw7qjo23PDr9V23Zfsgx2XNsQ7MN4NweIKl7K9gNaS4mSYIixC7vC0WgQBC6uLiSWRJC/ZfYlKkSWsAJ5K4bTQ2BMArbRVEe6HBArYFpBEWKbCklZVHBdu+x4c7Ow5ePP7C43tHsh7DpaAfXivbHtBSbuzWE3aDt5cFnLjjITR4thXZHXELt/ZT3CY1XQY72Sw1UyW5TxHyWm9lsoDKzSFEONxY80LV2qoxVAZvmrmqksS0R1PyWrGiuo0pJgdFdYBtoSOHpyOjj8TZW1Fuh4Jgzj/abs80iXMHhOvJcdiLmV7F2jhBVpOYdxbkV5PjMA6nUcx2oJjmFhzQrJnK2IFHw90u+m4BGwboF1zIpWOEZbparVlTxDyQgNiLqhaYRtRCz3U2BADDJSFoPnWJaFtFk2UuGkqzwtDU8it4djU1hW3PRCGoUxFjcuqg6CUxXb4oWGmE3rAkmDYNk52Th296M/uzxjylKCQeS6T2cw83IBkcgdeaqD7NIGrO/7IpgMEaevxV1RKpezvC0DRWDai79Fos2PRW1FXU6iaYeKkoeY5bLkqKyiak6FAWNGoFEvSpfHNSbUSAL3iWxtxKNKWxBmyQ0VTxqk6ok/AKzqU0uaUBIZlNgACO1wSueAp0npgNtqLmPa/B6VALaE8OqvHusfvdQrAPpua4WhDyqJaPM69DLfZJOKuO06WUkTIGh4qtsVwyVOhPQN1cZYWnU91utQ0ITFOnLCpV6ERY4WCC/Vbw8ArZbMlOwq0QyBYtZgsR3QupXGm5oMbJjs+sblMsoTm6SkDWygQNUaKk6YxUqSY32QaZvdaNTMQTZTr3uEei2h3DsDmmRorLsKsGkA3bPmDxCpS8tHVGw1WBKpSrIj1DCugazzTIqrnOxsXmYPF0GnwV5hxuV3p2hosKNWLrYxsmJVZicTeAtNxGX6qWzRIvBUssFYKqpYgxdHo1AbqiaLHPPRE74BJsxHDZBq4m6QbHnV50UZKDRqdFKpiTsih2SqvSdZ+y1VrlJurT9Uhm6h++SzPc8kF9X1UG1fqkA8XW8rqFF2oUKNSZHL4qLH78E0JnDe0NNwqGOPkefVU1RxaQuo7daKlS1jvGh/querDxHPsuPkVSZnLWDeHqyCCisrAAgbpTLwKE1rpWbbJthm0iJJWU3l1tFouc4QFGnUiyQ1VGv2Q8Vi335WJdV8CwFw1TxjgQlcRhxMBNsgHkDC3jQLEW5K7N5r+kr6eGMX6I2HpaNPqm2nwkqWaGTuISszSSyV9YyY4LJITGKAJJiPql3usB8Um6Jqy07GxGWoCLTqCuz/ALwt4Z6fVcDg8xIt9hdx2DQcG+IyeQHz3XV/Hk2qHHAeg1xMlp9LDqfonBTMTEfxH9Ajtp8z6oGJDjqfRbtUbJ2JOrS4ASQrB9XKIGpSdJhboi06J3SjYSDsrQPu6lSO5UBSWEwqoQy6vG6gK8pOo7msZXHkgQ5VcIulqr0F+KugVq+33Kl0Mj3ni5fNGe+OqGxqx7rhJJgMUXRK0ytqhMqwq7HVwGlsxMqtEtlD2g4mq4tsZv8A1QKrw9k67SoV8UcxM9Tx5pUVDltsdFwTabZKdKgtWA2N0TBuHulItrSRmsm3UYM5gRxClZ0Zp5sXqsdTe7gp4a/ijRNPp52zrCXeMpH2CqbKWxzKOCxRzjktp2XgSoyQ5p9eaUrMcWyTuAj94JBHmOqFVdBjYEEqGlRLY1hSchHA381s2ahV6uUgDfX9Ezg2AscDczbrumkDXhlOkXBxtAEk8EEEPIAEDb+qJXIayQTYgEcTuUNjQ4l7RBO36hJhGND7XFggbGx5Fdf2PXGQb/fJcK+rcGY0n5Lp+y6xb4J00PEcV0/x5Zo0kqydIa62HykGvRBiAAusSHmhbKVoV5Ri/mkhmOKXrOgKT6vBKVqnEoYIDUqlQNQ7boT6gPRDfWy3PksxhS+DzW+9AKrG4u8oP7WcwKXZAX78UBdLUsXM8v6qmxOLLiANvmpUK0I72xF3Wrxuub7dxpL8o3Nij4zFScm8T/RUrcQHEtPM850hY83JikQ12wFDmnqh03FrS4iRMLdWl4p0EWjX0UqdmtGxK5sNgRxFMWOxuFo1rQNN1a1MrybafRVb6QaTIj9USxoJRrQZpLbKHW4B04cwpupFwz6DL8UKq4RAGuqOxLVDUt/MFiR7g8CsSt/BVP4AUzBt+K458vmhF4c65GvwUXVQ2cvijy1US5sTG/6BVKREa0xljSamY6W9FYOrADu8wDnSbi0HadjCQwhHiJvliBsXHT6pPvD3gLzMuGaOeqelYOVD9UkEAiAL9RsZ31WqpIFpB25IpYWVHUh4qeZ0A8BJkcClH1PF4XDKRIJ/LeEnY2sWhjFQACd9eZ4BWXZfaRcAJyjS2pA2VI/FiQ5w2HpxHNFwNX95l1AuHaWiSnGdSwVJ28Hd0sQSOShVxE7rn8B2sIuYHD6pmvitL6rvU00CZeYevFpVj3hhcth8QZ5kq6p14HNUslB69bmq+q+Vuq/itNcAkxohoksYZPJNvfOgQH4V51WUn4UkIhw8ku96axWFI1QsMwOMKNjonTpiEGtUBAINtJ6R9VvtMlrcoHvaR99VXYd3gdTgzZxMaAED9VHJOsInY3VsMxbeS2fl8PkkyMzmuEXMHkrDDMBY9h1c1zx/oP0lU+Bf4iGjwukGdjsZ6rB/kmWNjFcDOTdO4AtyZX7XzcOvJIvE5CY3BjiNvktuxQyua47gjjGgCTBNN2houIJMwZ8gRotHEBx8Q8JP+08QoYeu0iNxETuBtKE5kOJbdrjmbOobvfkbKLwK0WbWOLCy3LnHBKinpOyzDYktuCCDsfuyZyAlwabxcHYblaJ4GqZnfH+H0WkL9g5/NYnb+B9plKzC5Kjhcxe/4m/hI6ypMrzYU2wJmZNtz5IzS97DBHeBzTqBLXSInlA/3KfalAwSIiACABfIAAemhPMhHW9GbVK0IUsXJLWuLRtAgkzqeCnTogObmMguHM+9eyWwVOo4mMpOgkgOGblMlWtRhFHxiH0yyALXk3NtCJ9E6FFN22AxWKuKkG7yeQZuD1n4JVlDwED/AMZLRP5XXB6T80bHUjlLBfLDMotJgSZ6yi4Wm/K4FsHu411i487BDRVW2hMMlgBjNsDaRaCDvup0CGB2YXAFgbxGo4FO06YfTa0tgtOUTzuD01+CWe12cyNAA6ePCehKn9CSUaZtxyybPbYyBoZFiNjdNvxFQm9wwgdBr8UN1LIYIs5tiL5hwURWnMCYzNIHIjSeafZodIt8NiYyzaZvy4hWWGxsz9x9hcvi8QWsZHvBt+Ak/duasKNSGtgk5m5g6LETBvtddXHy+MEXPf78Z9EFuKDnRP8A0qav2k33Q73ducaoPZuIkkk62+IlayZSO5w1KQFYsw9lW9kVvC2V0NFgIRRaZRdq4IupuLRcAn0XK06pYwuEE6dCeK9Gc0ea4P2uotpOLqZEVDD2cHEajhso5FSsTZXUKrn1MomAAZHGInlqj1KzW1JdwIMb9fRVmCxBa6Z18J42gny0W6jszGzIImRF823quO7M03aRbdlsBrZpA4zuHCDH+5U9VuV7mzGvqD/0mMYILSP4ARN3SBJbzkoONrguzRczBNuh5qWvGLkabofr4YPolwGhD40jOId8QFVNwrbh4O1x1vKtMF2i1jKmY2NMk2m8SL9Qg55pgmA4g/prCqkK6yItoFn4wSZ8rwPgj03WY0i+Yu6CDI9duqEwmMzhawsTtYkeY05rWNEERNzmB3H4T5RdSClUqQanWEFsD3jBH68f+lZYNviLnECabri87Sq0NygQN9DrBi3whPMrtptLiJOVugMnM4WjjY+ipZKj7ZZd6OPwK2qz+9f4T9+axMfZinZeVznAfkcAY3AmPLKFCqwuDZGanLh1gtI+I+SW7LoOpvzOdLJ1aZE9OYJHoiUWksfT0uC2+rrkieYHwVJYKWVoD3F83d2LSc02afkYhEp4lzrP8WYNtNvDOk6D6oJxQeRIsLGNAN4UajhMja42IEhS34ZqWRysJgZjqXEkRJtoeMypMcXMc4e8G7bEwPlKTrvbdpcRZtzxN7x8+IWYfG5HToCQHCJmJ34+LRMq92GoVw45sxlsEmbW/rB8yl34h7ibzmMiBB11H3uh4kBs21IBI0N5kDgdVDFVPCwC9nOsDMFwgRx5dVNWKSZKrUOhJ0Agzcxrrr9EzhfGxzs0BjC/l4SB56ylMgcJcczo0aZaL6kj5c032S8DvWRM4d54AkvYY9Iuq65JhHIDEVGuyEzYVBMzmNvEeN7xwCnh8US00wcpb423Hug/vAfg6P4TxVe+paBSmHySHGII1N+UeaZ/ai3MQA0uPAAkOAAM6/ZVqGcsvr2ZTdqUi/EQxx8TrGY812XadOnQZSe0GDT0H5hlEnhquQptOYEETJvrvxXb1njJRL2BzJe187FzDA8zb0XXJp4KlhDnY+NOVpmxC67Bdqt0K8wwva7GuyCwa7LfYLquxMtaXB7mhsTI1BnikmKLs6ytjGiCD6Lz32ixTXV6rpHvBgI3IZIPUaL0PD9lUS0Bwkj8QMHyXPe0HszmDg2GuJzBxENceJjQ8xwS5U3HBVWcLUBDZiSSJ02EW57qdMaRczEE6/co+Kwj6TxSqNg5cwc2bu3H6eQ4pYH95TiCCLG28xbhpPRcbiRWbYzSbYgjx0zAPRwiJ5GOq1iYdoJbcxvNxNvsqVbGRnB0JEO3aCJjpslW4m5EXA30IECLdfgpBpXXozUc1pZYubFwCGy3YGeBA9CtvqNiBpMtgzzNzprooxmoh3hzZzIN7Ec4iIt15omGYYDphpb+KCLRe3VFE9bwgbajSHOBcAXOmTIEm55i6iSDLhtp/LcT6D4c1DFVBkaWwWyRtqSTEcD+vJEwrSGPkwCC0wQPGbwCLREqdgoZMxOKBaBHNp4n8v3wRH0yaIdqXHOYJmwAmP8Acl2YbLN/DOp8s1hqBMdZTFQAHw7zY7a+oR+x9fkczj8o+/NbUI5n1/qsR2Zp0fz/AJKOi40wbmSIJHB2zuMxomBiphzSAQRoLAxEwdN/WFEYeXBp0IcQ42GbYELMRSNPw6kNE8S43cY21jyWtGMW0iOKaYz5chMNIFxnmDB4brX7O9jA51pjzJBm3p6wiYnEFobAgPAsZykzfMeoRcTDYiYJ93SDGsbhJoI1diGMdlOWOYOo035W0UKTnuIDT4pAiJDgYEX6pqvhy54M6C5E6cCh0qmVzXBrnTaxjLBILvTikgeJfsNVo5S3xSGO1aMwIbJLJ3IukqrXGjmaDDDeLktde53Fp80++oKJbHiBMzfSDI20CNhRTcHRbMADBgkTOoEHzhUqNPwVWDqZWMLZ96b24a8olM4em7PUaP8ALeZP5IE+uiM3AOcYBIsfELGOilhqUEjUmlUE2JH7sn0EfJJZ2R6hDA0PC8FxFi9o0925B4CJS1RxhpvmGsXkEE5fSUfCEtLXERLjnBsCHCPKRKM3ABsmScpbNtYBsONt1diT+BKqGgAga3k67G8JvtTGPNKASKZiRYi8EH4C6K3CSBAmnB6Rs6dQQbELT6QyQDmBaI0EEEmHDbcJfUY73ZS0W2BFxHQ8fmul7F9onMIaWgh2Vtpm5yz+qqaXZ4aJbIFvC7UEiZnhci4R+z8IWlsi/eNAk/lBc4m/3CpSpsFfY7rs/wBqaYlueHCo4QSJjvCePAq+xeIL2hwfAMASJkkwNOq8l7NcKmIpNIuarCHgCfG4EzxCb7R7axOaqC4CHuLb5QWtfLTOgbpw3W0ZpouM/Tp+2q1Njg6q4kNILniW2g6cLxoudxVbDvcXMqBhdpOgEWiLjTUpX2mxjqmFouyklzpqQczQcoy6atNyud7KwofVDYEElzjeWMaZJzDkD8E31ayU2mdt2jgy4sDMpORuYAgS7K2InbT1Stah3ZOZpIygEEe642LragfqJ50+NqNrPe+C0zYgu0BhsgHlqrs452R78v7wOY2Z96zgDfQmLjeFzyUVohtN/kVmB3ejiYsIE6i6de7wZWwe7aZAuPFEkDeMo9UNzxkmQ2obfwtNwQ0/hJBFvTgtdniGkOIBIjk5pETbTdZywODVipAu8NzAG97G+vWU7TpANDc9pJncyTaL3SuPploygGG300m467KdEOHiJMWMWB1PrcaIWybadVkNUZADwdIbr7ok+EnaZJ81Oi0nK57YzZr69AOcjVJUiW3kzaTsZ/UWVi5xLKYcfzPbaSfEQRyBIU7Gqu2b/aeT/Rv/ANLazvB/ln4fRYjoV9b8CVNrhLrHLcDXM4iwHEb+SAzEXh0kx7wkEHdro1HONlvHVpkWYJEaWPIcIWmkgmTJIuRqOnVW3TOdyp0EbUMhmaeB68OHVDxFSNCZsA6ZmBG6Caw3ncXlaw7Q513EAA8NQLAcVPtB2Itrlu8bk78I5KdLEQLnXMIjp8dUGoQTJF9HA8wmKtICxjlE/S6KFsFVqAFp94cTrupuqBkT+IzvadljqBAkeUxrrAWOGaxtMRI0ugTb9LLC1QWmqKhDmNyuBuOTgNLixRGYdr3tc4ywgtdlIESDDhHxQezXZQQQCDqI12jotPwoac1P3dPfgg/lLeSvzJr2bWBUYdhvMwAC2Z00jn9FYh37geEECwGnnOs+e6rcXlLiG6tIbGhB3P8AEn6HiotF4Et5+aVhx7YphMQWyyCBJjeJ1DuqkMMwEvzeCYLBJOnoBzUKlGCSRckT/T4LdCmCYiJMOjqNVCeSHuhiniIiGHuogzeefy04lMVKAyNykgAuIIAuHNgg8dUpXY/MZMNboCbZeHJRwlTIMpBc0njpoFawy1WgWDwjhiqTm+617bnTwm4t5pmvIY8CfES5x1IuSRPDki0WOpVQYmCTrpP/AGoYivljxWOoiepKSbKb6qw1EU8mRzM4LACWnKG224EESEvjuy2taclnVDlLt8usEcdJ42RsIweKoTNxlb+dxPDhop1HjOXTLs4LhsJGkb9U+zqxJ2heh2ZUOUBo8NgZiWjWdyNeSZfAplrHAvcJLpG0gMaNdzfVa7RxBM5DlBsZ1dHE8OSUbTa0wX3cCNN9evFLtWC6zZClQdUGWHTmEyNBHPVRwOF7xwph17kyILWje2o+qvKdIBjXsMggNc069eSHhKTSapb4HXaHQBAMGAn1vAdVVlPV7QbkDHfvGyADuNfdMWm1ijvomC5ozAWl0yBMwYMawhfsQAPhyuk5tvFxA56qVJrqeV1wHEAA2BAtB46lTSsTleRbv7uBsLgWibTbin6VQv2ADRkEGXBp4jbc+ahjqDPE6DqRA8TZBgEbt/qg0KRnNIuSc0kRwaQd/giqEreSz/ulv+Y30WJfu6vEfFYnj4Dq/wAFRjv8U+fyWYXQdB8isWKntnOwFfbqmho3z+axYsvUVx7Ywffd0+iK/wB0dVixWxmn6N80pT98df1W1iBT2M4T3j1C0zWp1+qxYiX2j4xTF6t/mH6K5o+4eo/RYsT8Kh/oFi9R0+iFT/F/N+oWLEobIjthq+lT+b6KvZ+Dr+qxYkD2XWL/AMZ38jPkFTYjXy/UrFiHo05ftRZs/wDD/q/5JGt77fP9FixVLQMa7Q2/lHzCXqajoP1WLFlL7y5Fx2X7hS2J91/VqxYtlob/ALaCP99n8o+SD2l/+Wj/AOx/zWLFHpK0xepqf/YP+TVGn/iH/UsWJz8Jh9v/AH4GFixYgD//2Q=="/></div>
                    </div>

                </div>
                <div class="details col-md-6">
                    <div class="panel panel-default text-center">
                        <h3><div class="panel-title"><span class="glyphicon glyphicon-list-alt"></span>Nama</div></h3>
                        <hr>
                    <h4>Ayam Kapas</h4>
                    </div>
                    <div class="panel panel-default text-center">
                    <div class="rating">
                        <h3><div class="panel-title"><span class="glyphicon glyphicon-info-sign"></span>  Category</div></h3>
                        <hr>
                        <h4>Ayam Unik</h4>
                    </div>
                    </div>
                    <div class="panel panel-default text-center">
                        <h3><div class="panel-title"><span class="glyphicon glyphicon-comment"></span>   Description</div></h3>
                        <hr>
                        <h4>Ayam berbulu seperti kapas</h4>
                    </div>
                    <div class="panel panel-default text-center">
                        <h3><div class="panel-title"><span class="glyphicon glyphicon-credit-card"></span>  Harga</div></h3>
                        <hr>
                        <h2><font color="purple">Rp. 110.000</h2>   </font>                 </div>
                    
                    <div class="text-center">
                        <button class="add-to-cart btn btn-default" type="button"><span class="glyphicon glyphicon-cart"></span> Beli</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
    </body>
</html>
