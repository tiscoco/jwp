<%-- 
    Document   : daftar
    Created on : Mar 28, 2019, 10:34:53 PM
    Author     : Tis Co
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Daftar</title>
    </head>
    <body>
       <div class="span3">
    <h2>Sign Up</h2>
    <form>
    <label>First Name</label>
    <input type="text" name="firstname" class="span3">
    <label>Last Name</label>
    <input type="text" name="lastname" class="span3">
    <label>Email Address</label>
    <input type="email" name="email" class="span3">
    <label>Username</label>
    <input type="text" name="username" class="span3">
    <label>Password</label>
    <input type="password" name="password" class="span3">
    <label><input type="checkbox" name="terms"> I agree with the <a href="#">Terms and Conditions</a>.</label>
    <input type="submit" value="Sign up" class="btn btn-primary pull-right">
    <div class="clearfix"></div>
    </form>
</div>
    </body>
</html>
