/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javabeans;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Validate", urlPatterns = {"/Validate"})
public class Validate extends HttpServlet {
    
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try {
          float tugas = Integer.parseInt(request.getParameter("tugas"));
          float uts = Integer.parseInt(request.getParameter("uts"));
          float uas = Integer.parseInt(request.getParameter("uas"));
       
            float niltgs = uts*30/100; 
            float niluts = uts*30/100; 
            float niluas = uts*40/100; 
            float total = niltgs+niluts+niluas; 
            
            if ( total>= 85 ){
                out.println(" Grade Anda A");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
            }
      
        else if ( (total >= 80) && (total < 85) ){
                   out.println("Grade Anda A-");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
        }else if ( (total >= 75) && (total < 80) ){
                out.println(" Grade Anda B+");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
                }
        else if ( (total >= 70) && (total < 75) )
        {
            out.println(" Grade Anda B");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
        }
        else if ( (total >= 65) && (total < 70) )
        {
            out.println(" Grade Anda B");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
        }
        else if ( (total >= 60) && (total < 65) )
        {
            out.println(" Grade Anda C");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
        }
        else if ( (total >= 45) && (total < 60) )
        {
            out.println(" Grade Anda D");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
        }
        else if ( total < 45 )
        {
            out.println(" Grade Anda E");
                             RequestDispatcher rd=
                             request.getRequestDispatcher("index.jsp");
                             rd.include(request, response);
        }
            
                             
        } finally {
            out.close();
        }
    }
}