

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ayam Shop</title>
        <link href="style.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"> 
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        
    
    </head>
    
    <body >
        <nav class="menum navbar navbar-light navbar-expand-md  absolute-top">
                <div class="container" >
                   <img src="logoyam.png" alt="" width='10%'>
                   <h1 class="judul">Ayam Shop</h1>
                <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
            
                 <ul class="topBotomBordersOut navbar-nav mx-auto text-center">
                    <li class="nav-item active">
                      <a class="nav-link" href="index.jsp">Home</a>
                    </li>
          
                    <li class="nav-item">
                        <div class="search-box">
                                <input type="text" class="search-txt" placeholder="Cari Disini">
                                <a class="search-btn">
                                    <i class="fas fa-search"></i>
                                </a>
                        </div>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="login.jsp">Masuk</a></li>
                    <li class="nav-item"> <a class="nav-link" href="daftar.jsp">Daftar</a> 
                    <li class="nav-item"><button type="button" class="btn btn-default btn-sm">
         <a href="cart.jsp" class="nav-link"> <span class="glyphicon glyphicon-shopping-cart">Keranjang</span></a> 
        </button></li>
                  </ul>
                </div>
                </div>   
              </nav>
  <div class="container pro">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="produk.jsp">
                        <img class="pic-1" src="https://i2.wp.com/okdogi.com/wp-content/uploads/2016/07/ayam-kapas.jpg">
                        <img class="pic-2" src="https://i2.wp.com/okdogi.com/wp-content/uploads/2016/07/ayam-kapas.jpg">
                    </a>
                    <a href="#" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Kapas</a></h3>
                    <div class="price">Rp. 110,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div> 
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="produk.jsp">
                        <img class="pic-1" src="http://pasadenavillagepuncak.com/wp-content/uploads/2018/06/Cara-Mudah-Membuat-Ayam-Kampung-Bertelur-Tanpa-Henti.jpg">
                        <img class="pic-2" src="http://pasadenavillagepuncak.com/wp-content/uploads/2018/06/Cara-Mudah-Membuat-Ayam-Kampung-Bertelur-Tanpa-Henti.jpg">
                    </a>
                    <a href="#" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star disable"></li>
                        <li class="fa fa-star disable"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Kampung</a></h3>
                    <div class="price">Rp. 75,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="produk.jsp">
                        <img class="pic-1" src="https://hargabinatang.com/wp-content/uploads/2018/11/harga-ayam-arab.jpg">
                        <img class="pic-2" src="https://hargabinatang.com/wp-content/uploads/2018/11/harga-ayam-arab.jpg">
                    </a>
                    <a href="#" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Arab</a></h3>
                    <div class="price">Rp. 125,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="#">
                        <img class="pic-1" src="https://jualayamhias.com/wp-content/uploads/2015/03/Ayam-Pelung.jpg">
                        <img class="pic-2" src="https://jualayamhias.com/wp-content/uploads/2015/03/Ayam-Pelung.jpg">
                    </a>
                    <a href="produk.jsp" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Kedu</a></h3>
                    <div class="price">Rp. 105,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div>
    </div>
       <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="produk.jsp">
                        <img class="pic-1" src="https://i2.wp.com/okdogi.com/wp-content/uploads/2016/07/ayam-kapas.jpg">
                        <img class="pic-2" src="https://i2.wp.com/okdogi.com/wp-content/uploads/2016/07/ayam-kapas.jpg">
                    </a>
                    <a href="#" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Kapas</a></h3>
                    <div class="price">Rp. 110,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div> 
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="produk.jsp">
                        <img class="pic-1" src="http://pasadenavillagepuncak.com/wp-content/uploads/2018/06/Cara-Mudah-Membuat-Ayam-Kampung-Bertelur-Tanpa-Henti.jpg">
                        <img class="pic-2" src="http://pasadenavillagepuncak.com/wp-content/uploads/2018/06/Cara-Mudah-Membuat-Ayam-Kampung-Bertelur-Tanpa-Henti.jpg">
                    </a>
                    <a href="#" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star disable"></li>
                        <li class="fa fa-star disable"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Kampung</a></h3>
                    <div class="price">Rp. 75,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="produk.jsp">
                        <img class="pic-1" src="https://hargabinatang.com/wp-content/uploads/2018/11/harga-ayam-arab.jpg">
                        <img class="pic-2" src="https://hargabinatang.com/wp-content/uploads/2018/11/harga-ayam-arab.jpg">
                    </a>
                    <a href="#" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Arab</a></h3>
                    <div class="price">Rp. 125,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="product-grid9">
                <div class="product-image9">
                    <a href="#">
                        <img class="pic-1" src="https://jualayamhias.com/wp-content/uploads/2015/03/Ayam-Pelung.jpg">
                        <img class="pic-2" src="https://jualayamhias.com/wp-content/uploads/2015/03/Ayam-Pelung.jpg">
                    </a>
                    <a href="produk.jsp" class="fa fa-search product-full-view"></a>
                </div>
                <div class="product-content">
                    <ul class="rating">
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                        <li class="fa fa-star"></li>
                    </ul>
                    <h3 class="title"><a href="#">Ayam Kedu</a></h3>
                    <div class="price">Rp. 105,000</div>
                    <a class="add-to-cart" href="">Lihat Produk</a>
                </div>
            </div>
        </div>
    </div>
</div>
         
          <footer class="footer">
            
      <div class="container">
        
 <ul class="social-icon animate pull-right">
                  <li><a href="#" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a></li> <!-- change the link to social page and edit title-->
                  <li><a href="#" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#" title="google plus" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                </ul>
                </div>
                 
    </footer>
</html>
